const express = require("express");
const router = express.Router();
const { register, login } = require("../controllers/userController");
const validate = require("../middleware/validate");
const { registerRules } = require("../validators/rule");

router.post("/register", validate(registerRules), register);
router.post("/login", login);

module.exports = router;
